(function() {
  const max_row = 20;
  const max_col = 10;
  const blocks = {
    i: [
      [1, 1, 1, 1]
    ],
    o: [
      [1, 1],
      [1, 1]
    ],
    t: [
      [0, 1, 0],
      [1, 1, 1]
    ],
    s: [
      [0, 1, 1],
      [1, 1, 0]
    ],
    z: [
      [1, 1, 0],
      [0, 1, 1]
    ],
    j: [
      [1, 0, 0],
      [1, 1, 1]
    ],
    l: [
      [0, 0, 1],
      [1, 1, 1]
    ]
  };
  const blockKeys = Object.keys(blocks);
  var main = document.getElementById("main");
  var hello_text = document.getElementById("hello_text");

  var cells = [];
  initTable();

  var isFalling = false;
  var fallingBlockNum = 0;

  hello_text.textContent = "初めてのJavaScript";

  var count = 0;
  var drawer = setInterval(function() {
    count++;
    hello_text.textContent = `初めてのJavaScript(${count})`;
    if(hasFallingBlock()) {
      fallBlocks();
    } else {
      deleteRow();
      if(!generateBlock()) {
        alert("Game Over");
        clearInterval(drawer);
      }
    }
  }, 1000);

  document.addEventListener("keydown", function() {
    if(event.keyCode === 37) {
      moveLeft();
    } else if(event.keyCode === 39) {
      moveRight();
    }
  });


  /**
   * テーブルを描画し、td_arrayでセルを管理する
   */
  function initTable() {
    var fragment = document.createDocumentFragment();
    for(let row = 0; row < max_row; row++) {
      let tr = document.createElement("tr");
      fragment.appendChild(tr);
      for(let col = 0; col < max_col; col++) {
        let td = document.createElement("td");
        tr.appendChild(td);
      }
    }
    main.appendChild(fragment);

    var td_array = document.getElementsByTagName("td");
    for(let row = 0; row < max_row; row++) {
      cells[row] = [];
      for(let col = 0; col < max_col; col++) {
        cells[row][col] = td_array[max_col * row + col];
      }
    }
  }

  /**
   * ブロックを落下させる
   */
  function fallBlocks() {
    for(let col = 0; col < max_col; col++) {
      if(cells[max_row - 1][col].blockNum === fallingBlockNum) {
        isFalling = false;
        return;
      }
    }
    for(let row = max_row-2; row >= 0; row--) {
      for(let col = 0; col < max_col; col++) {
        if(cells[row][col].blockNum === fallingBlockNum) {
          if(cells[row + 1][col].className !== "" && cells[row + 1][col].blockNum !== fallingBlockNum) {
            isFalling = false;
            return;
          }
        }
      }
    }
    for(let row = max_row-2; row >= 0; row--) {
      for(let col = 0; col < max_col; col++) {
        if(cells[row][col].blockNum === fallingBlockNum) {
          cells[row + 1][col].className = cells[row][col].className;
          cells[row + 1][col].blockNum  = cells[row][col].blockNum;
          cells[row][col].className = "";
          cells[row][col].blockNum  = null;
        }
      }
    }
  }

  /**
   * 落下中のブロックがあるか
   * @return {Boolean} 落下中のブロックがあればtrue、なければfalse
   */
  function hasFallingBlock() {
    return isFalling;
  }

  /**
   * 揃っている行を消す
   */
  function deleteRow() {
    var canDelete;
    for(let row = max_row-1; row >= 0; row--) {
      canDelete = true;
      for(let col = 0; col < max_col; col++) {
        if(cells[row][col].className === "") {
          canDelete = false;
        }
      }
      if(canDelete) {
        for(let col = 0; col < max_col; col++) {
          cells[row][col].className = "";
        }
        for(let downRow = row - 1; downRow >= 0; downRow--) {
          for(let col = 0; col < max_col; col++) {
            cells[downRow + 1][col].className = cells[downRow][col].className;
            cells[downRow + 1][col].blockNum  = cells[downRow][col].blockNum;
            cells[downRow][col].className = "";
            cells[downRow][col].blockNum = null;
          }
        }
      }
    }
  }

  /**
   * ランダムにブロックを生成する
   */
  function generateBlock() {
    var nextBlockKey = blockKeys[Math.floor(Math.random() * blockKeys.length)];
    var nextBlock = blocks[nextBlockKey];
    for(let row = 0; row < nextBlock.length; row++) {
      for(let col = 0; col < nextBlock[row].length; col++) {
        if(nextBlock[row][col] && cells[row][col + 3].className !== "") return false;
      }
    }

    fallingBlockNum++;
    for(let row = 0; row < nextBlock.length; row++) {
      for(let col = 0; col < nextBlock[row].length; col++) {
        if(nextBlock[row][col]) {
          cells[row][col + 3].className = nextBlockKey;
          cells[row][col + 3].blockNum  = fallingBlockNum;
        }
      }
    }
    isFalling = true;
    return true;
  }

  /**
   * ブロックを右に移動する
   */
  function moveRight() {
    if(!canMove("right")) return;
    for(let row = 0; row < max_row; row++) {
      for(let col = max_col-1; col >= 0; col--) {
        if(cells[row][col].blockNum === fallingBlockNum) {
          cells[row][col + 1].className = cells[row][col].className;
          cells[row][col + 1].blockNum  = cells[row][col].blockNum;
          cells[row][col].className = "";
          cells[row][col].blockNum  = null;
        }
      }
    }
  }

  /**
   * ブロックを左に移動させる
   */
  function moveLeft() {
    if(!canMove("left")) return;
    for(let row = 0; row < max_row; row++) {
      for(let col = 0; col < max_col; col++) {
        if(cells[row][col].blockNum === fallingBlockNum) {
          cells[row][col - 1].className = cells[row][col].className;
          cells[row][col - 1].blockNum  = cells[row][col].blockNum;
          cells[row][col].className = "";
          cells[row][col].blockNum  = null;
        }
      }
    }
  }

  /**
   * 左右に移動できるかどうか
   * @param {String} mode 右に動かしたいのか、左に動かしたいのか
   * @return {Boolean} 移動できればtrue, できなければfalse
   */
  function canMove(mode) {
    var judge, diff;
    if(mode === "right") {
      collision_wall = function(col){return col == max_col - 1};
      diff = 1;
    } else if(mode === "left") {
      collision_wall = function(col){return col == 0};
      diff = -1;
    }

    for(let row = 0; row < max_row; row++) {
      for(let col = 0; col < max_col; col++) {
        if(cells[row][col].blockNum === fallingBlockNum) {
          if(collision_wall(col) || (cells[row][col+diff].className !== "" && cells[row][col+diff].blockNum !== fallingBlockNum)) {
            return false;
          }
        }
      }
    }
    return true;
  }
}());
